"""
ASGI config for the Charmed Django project.
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "application.settings")

application = get_asgi_application()
