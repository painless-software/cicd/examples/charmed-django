# Charmed Django

[![PyPI][package-badge]][package-link]
[![Docker][container-badge]][container-link]

A Django project deployed as a Juju Charm.

This project contains the application source code and builds the Python
package `charmed-django` as well as the homonymous container image. See
the [Releases][releases] section in the contribution guide for details.

## Getting Started

See [CONTRIBUTING][contrib] for instructions on development and testing.

[package-badge]: https://img.shields.io/badge/package-charmed--django-blue?logo=pypi&logoColor=yellow
[package-link]: https://gitlab.com/painless-software/cicd/examples/charmed-django/-/packages
[container-badge]: https://img.shields.io/badge/container-charmed--django-blue?logo=docker
[container-link]: https://gitlab.com/painless-software/cicd/examples/charmed-django/container_registry
[releases]: CONTRIBUTING.md#releases
[contrib]: CONTRIBUTING.md
