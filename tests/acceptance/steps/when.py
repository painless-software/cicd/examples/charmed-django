"""
'When' step implementations for acceptance tests. Powered by behave.
"""


@when("I log in with username and password")
def step_impl(context):
    assert True, "Implementation of a test action expected."
