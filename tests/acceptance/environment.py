"""
Environment setup for acceptance tests. Powered by behave.
https://behave.readthedocs.io/en/latest/tutorial.html#environmental-controls
"""


def before_all(context):
    """Before the whole shooting match."""


def after_all(context):
    """After the whole shooting match."""


def before_step(context, step):
    """Before every step."""


def after_step(context, step):
    """After every step."""


def before_scenario(context, scenario):
    """Before each scenario."""


def after_scenario(context, scenario):
    """After each scenario."""


def before_feature(context, feature):
    """Before each feature file is exercised."""


def after_feature(context, feature):
    """After each feature file is exercised."""


def before_tag(context, tag):
    """Before a section tagged with a given name."""


def after_tag(context, tag):
    """After a section tagged with a given name."""
