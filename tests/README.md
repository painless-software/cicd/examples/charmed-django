# Tests

> Software without tests is broken by design.
>
> -- https://painless.software/writing-tests

## Philosophy

No need to install anything for running tests, apart from [Tox][tox].

- All dependencies for running your tests are specified in [tox.ini][ini].
- Tox will create virtual environments and install those packages
  automagically when running your tests.
- The same commands will run in your [CI/CD pipeline][pipeline], just
  like on your local machine.

See [CONTRIBUTING][contrib] for instructions on working with Tox.

[tox]: https://tox.wiki/
[ini]: ../tox.ini
[pipeline]: ../.gitlab-ci.yml
[contrib]: ../CONTRIBUTING.md
