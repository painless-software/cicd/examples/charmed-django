"""
Tests for the REST API's URL structure.
"""

from http import HTTPStatus

from django.http import HttpResponseRedirect


def test_home(client):
    """Does the API app show the DRF API root page by default?"""
    context = {"format": "api"}
    response = client.get("/api/v1/", context)
    page = response.content.decode()

    assert response.status_code == HTTPStatus.OK
    assert "Django REST framework" in page
    assert "Api Root" in page
    assert "The default basic root view for DefaultRouter" in page
    assert "Log in" in page


def test_login(client):
    """Does requesting API login show the login form?"""
    response = client.get("/api/login/")
    page = response.content.decode()

    assert response.status_code == HTTPStatus.OK
    assert "Django REST framework" in page
    assert "Username:" in page
    assert "Password:" in page
    assert "Log in" in page


def test_logout(client):
    """Does an API logout redirect to logout confirmation page?"""
    context = {"site_title": "Foo Bar"}
    response = client.get("/api/logout/", context)

    assert response.status_code == HTTPStatus.FOUND
    assert type(response) is HttpResponseRedirect
    assert response.url == "/api/logout/"
