"""
Unit tests for your Django project settings.
"""


def test_settings(settings):
    """Some features and settings should be absent in production."""
    assert settings.DEBUG is False
    assert settings.TESTING == "pytest"
    assert "debug_toolbar" not in settings.INSTALLED_APPS
    assert "debug_toolbar.middleware.DebugToolbarMiddleware" not in settings.MIDDLEWARE
    assert "django_browser_reload" not in settings.INSTALLED_APPS
    assert (
        "django_browser_reload.middleware.BrowserReloadMiddleware"
        not in settings.MIDDLEWARE
    )
