"""
Unit tests for the Django project urls.
"""


def test_urls():
    """Some URLs for development should be absent in production."""
    from application.urls import urlpatterns

    patterns = [str(_.pattern) for _ in urlpatterns]

    assert "admin/" in patterns
    assert "api/" in patterns
    assert "" in patterns
    assert "__reload__/" not in patterns
    assert "__debug__/" not in patterns
