"""
Unit tests for the application's asgi integration.
"""

from django.core.handlers.asgi import ASGIHandler


def test_asgi():
    """Exercise the ASGI integration code."""
    from application.asgi import application

    assert type(application) is ASGIHandler
