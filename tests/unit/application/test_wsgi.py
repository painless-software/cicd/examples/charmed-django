"""
Unit tests for the application's wsgi integration.
"""

from django.core.handlers.wsgi import WSGIHandler


def test_wsgi():
    """Exercise the WSGI integration code."""
    from application.wsgi import application

    assert type(application) is WSGIHandler
