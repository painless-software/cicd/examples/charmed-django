"""
Tests for the Django project's ``manage.py`` script.
"""

from unittest.mock import patch

from cli_test_helpers import shell


@patch("django.core.management.execute_from_command_line")
def test_manage(mock_execute):
    """Exercise the project's ``manage.py`` script code."""
    from application import manage

    manage.main()

    assert mock_execute.called


def test_entrypoint():
    """Is the ``manage.py`` entrypoint script installed properly?"""
    result = shell("manage.py")

    assert result.exit_code == 0
    assert "Available subcommands:" in result.stdout
