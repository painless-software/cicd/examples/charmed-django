# GitLab CI - pipeline for packaging (Python package)
---
include:
- local: .gitlab/ci/.stubs.yml

stages:
- codestyle
- safety
- test
- build
- publish
- trigger

tooling:
  stage: codestyle
  extends: .megalinter
  variables:
    FLAVOR: documentation
  only:
  - merge_requests
  - main

format:
  stage: codestyle
  extends: .tox
  variables:
    TOXENV: format
  only:
  - merge_requests
  - main

lint:
  stage: codestyle
  extends: .tox
  variables:
    TOXENV: lint
  only:
  - merge_requests
  - main

audit:
  stage: safety
  extends: .tox
  variables:
    TOXENV: audit
  only:
  - merge_requests
  - main

mypy:
  stage: safety
  extends: .tox
  variables:
    TOXENV: mypy
  only:
  - merge_requests
  - main

requirements:
  stage: safety
  extends: .tox
  variables:
    TOXENV: requirements
  allow_failure: true
  only:
  - merge_requests
  - main

pytest:
  stage: test
  extends: .tox
  variables:
    TOXENV: py
  parallel:
    matrix:
    - PYTHON_VERSION:
      - '3.10'
      - '3.11'
      - '3.12'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: tests/junit-report.xml
  coverage: /^TOTAL.+?(\d+\%)$/
  only:
  - merge_requests
  - main

behave:
  stage: test
  extends: .tox
  variables:
    TOXENV: behave
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: tests/TESTS-*.xml
  only:
  - merge_requests
  - main

package:
  stage: build
  extends: .tox
  variables:
    TOXENV: package
  only:
  - merge_requests
  - main

pypi-local:
  stage: publish
  extends: .tox
  variables:
    GIT_TAG: ${CI_COMMIT_TAG}
    TWINE_USERNAME: gitlab-ci-token
    TWINE_PASSWORD: ${CI_JOB_TOKEN}
    TWINE_REPOSITORY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi
  script:
  - tox -qe ensure_version_matches -- ${GIT_TAG}
  - tox -e package -- upload
  only:
  - tags
