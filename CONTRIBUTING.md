# Contributing

To start developing on this project it's recommended to use Docker Compose,
which sets up all dependent services and configuration for developing,
automatically.

1. Install Docker on your computer. (Docker Compose v2 is part of Docker.)
1. Copy `docker-example.env` to `.env`, and fill in secrets in that file.
1. Run Compose from the terminal to perform Django commands and start the
   entire setup, e.g.

```shell
docker compose build
docker compose run --rm application manage.py collectstatic --noinput
docker compose up
```

If your repository is private you'll need a personal access token, which
you can generate in your user settings, usually. To pull a container or a
Python package from your registries, you may need to log in first.

```shell
docker login registry.gitlab.com/painless-software/cicd/examples/charmed-django
docker compose pull
```

Shut down the setup by pressing <kbd>Ctrl</kbd>+<kbd>C</kbd>. To clean up
after you run e.g.

```shell
docker compose down
docker volume rm charmed-django_database-data
```

If you prefer to do it all yourself, locally:

1. Install Postgres and provide a database `postgres`, user `postgres`,
   password `postgres`.
1. Make an editable install of this project, which will install both the
   project (and development) dependencies and the `manage.py` script
   (see below).
1. Set `DJANGO_DEBUG` to `true`, and optionally `DJANGO_TESTING` to
   `pytest` or `behave`, before you launch the Django runserver.

Open your web browser at http://localhost:8000 to see the application
you're developing. Log output will be displayed in the terminal, as usual.

## Prerequisites

Install [Tox][tox] on your development machine, e.g.

```shell
python3 -m pip install tox
```

For Tox to be able to build the project you will need some system
dependencies in addition, e.g. on Ubuntu distros:

```shell
sudo apt-get install python3-venv
sudo apt-get install libpq5 libpq-dev
```

See the [`Dockerfile`](Dockerfile) for more details.

## Local Development

If you need to work interactively you can use [Tox][tox] to provision a
virtual environment with the dependencies it uses for running tests, e.g.

```shell
tox devenv                 # create virtual environment `venv`
source venv/bin/activate   # activate virtual environment
pip install -e '.[dev]'    # editable install with dev dependencies
```

You can now run any Python, Django or test command.

For interactive Django development set the environment variable
`DJANGO_DEBUG` to `true`, and optionally `DJANGO_TESTING` to `pytest`
or `behave`, and run Django (almost) as usual:

```shell
export DJANGO_DEBUG=true
manage.py migrate
manage.py runserver
```

If you want to use a database other than the configured default you can
use the `DJANGO_DATABASE_URL` environment variable, e.g.

```shell
export DJANGO_DATABASE_URL=sqlite:///sqlite.db
```

To leave the virtual environment, execute:

```shell
deactivate
```

You can update the HTMX JavaScript included in the frontend app using
the `htmx` Tox environment. It downloads the script from a CDN.

```shell
tox run -e htmx
```

## Testing

This project uses [Tox][tox] for managing test environments. Use the
`tox` command in the terminal to run your tests and checks locally:

```shell
tox list                 # list available environments
tox run -e lint,behave   # run a few environments
tox run -e py            # run tests with local default Python
tox                      # run entire suite
```

If you need to use command arguments for a command executed by a Tox
environment separate them from the Tox command with a double-dash, e.g.

```shell
tox run -e behave -- --stop
tox run -e py -- -vv --exitfirst
tox run -e lint -- . --fix
tox run -e format -- --help
```

## Development Process

### Branching Model

This project implements a process with simple, powerful topic branching
and optional review apps. You're encouraged to use `main` as your default
branch.

1. Start a topic branch (e.g. `feature/awesome-thing`, `fix/query-bug`).
1. Make code changes and add related tests.
1. Open a merge request (MR). GitLab will [mark test coverage results][cov]
   in the file diff view.
1. The CI/CD pipeline builds a [review package][gitlab-packages] and a
   [review image][gitlab-registry], and deploys a [review app][review-app]
   in your dev/test environment to allow hands-on verification. 🚀 (optional)
1. Merge the MR after code review and the optional manual verification.
1. The CI/CD pipeline tears down the review app, builds the
   [edge image][gitlab-registry] from the default branch and deploys
   the application to your staging environment. 🚀 (optional)

### Releases

For deployments to production you're encouraged to release a stable
version, preferably using [SemVer][semver] or [CalVer][calver] as a
versioning scheme.

1. Bump the [package version][package-version] and
   [create a release with a new tag][gitlab-releases] matching the
   package version.
1. The CI/CD pipeline will build a Python package and push it to the
   [local package registry][gitlab-packages] as well as the stable image
   and push it to the [local container registry][gitlab-registry]. 🚀

[tox]: https://tox.wiki/
[cov]: https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html
[review-app]: https://about.gitlab.com/stages-devops-lifecycle/review-apps/
[semver]: https://semver.org/
[calver]: https://calver.org/
[package-version]: pyproject.toml#L7
[gitlab-releases]: https://gitlab.com/painless-software/cicd/examples/charmed-django/-/releases
[gitlab-packages]: https://gitlab.com/painless-software/cicd/examples/charmed-django/-/packages
[gitlab-registry]: https://gitlab.com/painless-software/cicd/examples/charmed-django/container_registry
