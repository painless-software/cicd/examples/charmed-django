"""
URL configuration for API app.
"""

from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r"users", views.UserViewSet)

urlpatterns = [
    path("v1/", include(router.urls)),
    path("", include("rest_framework.urls", namespace="rest_framework")),
]
