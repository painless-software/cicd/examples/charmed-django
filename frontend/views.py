"""
Views for frontend app.
"""

from django.conf import settings
from django.shortcuts import render


def index(request):
    context = {"lang": settings.LANGUAGE_CODE}
    return render(request, "frontend/index.html", context)
