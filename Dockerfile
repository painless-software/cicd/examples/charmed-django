ARG DOCKER_REGISTRY=docker.io

FROM ${DOCKER_REGISTRY}/library/python:3.10-slim-bullseye

ARG BUILD_DEPENDENCIES="build-essential libpq-dev python3-dev"
ARG DEBIAN_FRONTEND=noninteractive
ARG DJANGO_SECRET_KEY=insecure-build-only-key
ARG PACKAGE=charmed-django
ARG PIP_INDEX_URL=https://pypi.org/simple
ARG PIP_NO_CACHE_DIR=1

ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PYTHONIOENCODING=UTF-8
ENV PYTHONUNBUFFERED=1
ENV USER=python

WORKDIR /app

COPY entrypoint.sh requirements.txt ./

RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
    ${BUILD_DEPENDENCIES} \
    libpq5 \
 && pip install ${PACKAGE} \
 && apt-get purge --autoremove -y ${BUILD_DEPENDENCIES} \
 && apt-get autoremove --purge -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && manage.py collectstatic --noinput --link \
 && useradd -u 1000 -U ${USER} \
# Support arbitrary user IDs (OpenShift guidelines)
 && chown -R ${USER}:0 /app \
 && chmod -R g=u       /app

USER ${USER}

HEALTHCHECK --interval=1m --timeout=5s --retries=2 --start-period=10s \
  CMD python -c 'from urllib import request as r; f = r.urlopen("http://localhost:8000/healthz"); raise SystemExit(0 if f.status == 200 else f"{f.status} {f.reason}")'

ENTRYPOINT ["./entrypoint.sh"]

ENV GUNICORN_CMD_ARGS="-b 0.0.0.0:8000 -k gthread -w 2 --worker-tmp-dir /dev/shm --threads 4 --log-level info --access-logfile - --error-logfile -"

CMD ["gunicorn", "application.wsgi"]
